
const { Leds } = require('node-sense-hat');
/*
// Mock for local testing
const Leds = {
  clear: () => console.log('clear'),
  setPixel: (x, y, z) => console.log('setPixel', x, y, z),
};
*/

const BINARY = 2;

const fill8Binary = (x) => x.toString(BINARY).padStart(8, '0');

const outputBinaryStrings = (hrs, mins, secs) => {
  const hX = 1;
  const mX = 3;
  const sX = 5;

  Leds.clear();

  const tasks = [
    { x: hX, color: [0, 0, 255], timeStr: fill8Binary(hrs) },
    { x: mX, color: [0, 255, 0], timeStr: fill8Binary(mins) },
    { x: sX, color: [255, 0, 0], timeStr: fill8Binary(secs) }
  ];

  tasks.forEach(t => {
    t.timeStr.split('').forEach((c, y) => {
      if (c === '1') {
        Leds.setPixel(y, t.x, t.color)
      }
    })
  })
}

function updateClock() {
  const d = new Date()
  outputBinaryStrings(d.getHours(), d.getMinutes(), d.getSeconds())
}

setInterval(updateClock, 500);
