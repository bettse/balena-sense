import os
import time
from sense_hat import SenseHat
from flask import Flask, request

app = Flask(__name__)
sense = SenseHat()

sense.clear()
sense.show_message('online')
time.sleep(2)
sense.clear()

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/message', methods=['POST'])
def show_message():
    content = request.get_json()
    if 'message' in content:
        print('message %s' % content['message'])
        sense.show_message(content['message'])
    if 'low_light' in content:
        print('set low_light to %s' % content['low_light'])
        sense.low_light = content['low_light']
    return "OK"

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
