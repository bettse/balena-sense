require('dotenv').config()
const os = require('os');
const noble = require('@abandonware/noble');
const {InfluxDB, Point} = require('@influxdata/influxdb-client')

const MAX_REASONABLE = 120;

const {DEBUG, INFLUXDB_V2_BUCKET, INFLUXDB_V2_TOKEN, INFLUXDB_V2_ORG, INFLUXDB_V2_URL} = process.env;
const host = os.hostname();

const writeApi = new InfluxDB({url: INFLUXDB_V2_URL, token: INFLUXDB_V2_TOKEN}).getWriteApi(INFLUXDB_V2_ORG, INFLUXDB_V2_BUCKET, 'ns')
writeApi.useDefaultTags({host})

const watchdogInterval = 5 * 1000;
const watchdogTimeout = 5 * 60 * 1000;
var lastSend = Date.now();

setInterval(metricWatchdog, watchdogInterval);

function metricWatchdog() {
  if (Date.now() - lastSend > watchdogTimeout) {
    console.log(`No metrics sent since ${lastSend}, exiting`);
    return process.exit(0);
  }
}

function main() {
  try {
    noble.on('stateChange', function(state) {
      if (state === 'poweredOn') {
        noble.startScanning([], true);
      } else {
        noble.stopScanning();
      }
    });
    noble.on('discover', onDiscover);
  } catch (err) {
    console.log('caught error in main', err);
  }
}

async function onDiscover(peripheral) {
  const {advertisement} = peripheral;
  const {localName, serviceData, manufacturerData} = advertisement;
  if (!localName) {
    // Ignore missing name
    return;
  }
  var points = [];
  if (
    manufacturerData &&
    manufacturerData[0] == 0x90 &&
    manufacturerData[1] == 0x05
  ) {
    points = puckMetrics(localName, manufacturerData);
  }
  if (
    manufacturerData &&
    manufacturerData[0] == 0x33 &&
    manufacturerData[1] == 0x01
  ) {
    points = blueMaestroMetrics(localName, manufacturerData);
  }
  if (
    manufacturerData &&
    manufacturerData[0] === 0xd2 &&
    manufacturerData[1] === 0x00
  ) {
    points = aSensorMetrics(localName, manufacturerData);
  }
  if (
    serviceData &&
    serviceData[0] &&
    serviceData[0].data &&
    serviceData[0].data[0] == 0xab &&
    serviceData[0].data[1] == 0x03
  ) {
    points = absensorN03Metrics(localName, serviceData[0].data);
  }

  if (points && points.length > 0) {
    if (DEBUG) {
      points.map(console.log);
      lastSend = Date.now();
    } else {
      try {
        await writeApi.writePoints(points)
        lastSend = Date.now();
      } catch (err) {
        console.log('Error writing to influx', err);
        // Exit process to cause restart of container
        return process.exit(69);
      }
    }
  }
}

function puckMetrics(localName, manufacturerData) {
  const json = manufacturerData.slice(2).toString();
  try {
    const payload = JSON.parse(json);
    const {doorOpen} = payload;
    const point = new Point('doorOpen')
      .tag(name, localName)
      .tag('device', 'Puck.js')
      .intField('value', doorOpen ? 1 : 0)
    return [point];
  } catch (err) {
    console.log('puckMetrics', JSON.stringify(err));
    return [];
  }
}

function aSensorMetrics(localName, manufacturerData) {
  const sensor = {
    name: localName,
    temp_c: manufacturerData[9],
    temp_f: (manufacturerData[9] * 9) / 5 + 32,
    inMotion: manufacturerData[10] === 1,
    x: manufacturerData.readInt8(11),
    y: manufacturerData.readInt8(12),
    z: manufacturerData.readInt8(13),
    currentDuration: manufacturerData[15],
    previousDuration: manufacturerData[15],
    battery: manufacturerData[16],
  };

  const t = new Point('temperature')
    .tag('name', localName)
    .tag('device', 'aSensor')
    .floatField('fahrenheit', sensor.temp_f)
    .floatField('celsius', sensor.temp_c);
  const m = new Point('motion')
    .tag('name', localName)
    .tag('device', 'aSensor')
    .booleanField('inMotion', sensor.inMotion)
    .floatField('x', sensor.x)
    .floatField('y', sensor.y)
    .floatField('z', sensor.z)
    .floatField('currentDuration', sensor.currentDuration || 0.0);
    .floatField('previousDuration', sensor.previousDuration || 0.0);
  const b = new Point('battery')
    .tag('name', localName)
    .tag('device', 'aSensor')
    .floatField('value', sensor.battery)

  return [t, m, b];
}

function blueMaestroMetrics(localName, manufacturerData) {
  // 33 01 0d 64 0e 10 00 37 00 d6 00 00 00 00 01 00
  // I see other advertisements with the same prefix (0x33 0x01) x
  // but the rest of the format doesn't match. So I use version to ignore them
  const version = manufacturerData[2];
  if (version === 13) {
    // basic temp sensor, the one I have)
    const battery = manufacturerData[3];
    const interval = manufacturerData.readUInt16BE(4);
    const counter = manufacturerData.readUInt16BE(6);
    const temperature = manufacturerData.readUInt16BE(8) / 10.0;
    const humidity = manufacturerData.readUInt16BE(10);
    const mode = manufacturerData[14];
    var fahrenheit, celsius;
    if (mode > 100) {
      fahrenheit = temperature;
      celsius = ((fahrenheit - 32) * 5) / 9;
    } else {
      celsius = temperature;
      fahrenheit = (celsius * 9) / 5 + 32;
    }

    const t = new Point('temperature')
      .tag('name', localName)
      .tag('device', 'blueMaestro')
      .floatField('fahrenheit', fahrenheit)
      .floatField('celsius', celsius);

    const b = new Point('battery')
      .tag('name', localName)
      .tag('device', 'blueMaestro')
      .floatField('value', sensor.battery)

    return [t, b]
  }
  return [];
}

// https://wiki.aprbrother.com/en/ABSensor.html#absensor-n03
function absensorN03Metrics(localName, serviceData) {
  const battery = serviceData[8];
  const celsius = serviceData.readUInt16LE(9) / 8;
  const humidity = serviceData.readUInt16LE(11) / 2;
  const light = serviceData.readUInt16LE(13);
  const fahrenheit = (celsius * 9) / 5 + 32;

  const t = new Point('temperature')
    .tag('name', localName)
    .tag('device', 'abSensorN03')
    .floatField('fahrenheit', fahrenheit)
    .floatField('celsius', celsius);

  const h = new Point('humidity')
    .tag('name', localName)
    .tag('device', 'abSensorN03')
    .floatField('value', humidity);

  const l = new Point('light')
    .tag('name', localName)
    .tag('device', 'abSensorN03')
    .floatField('value', light);

  const b = new Point('battery')
    .tag('name', localName)
    .tag('device', 'abSensorN03')
    .floatField('value', battery);

  if (fahrenheit < MAX_REASONABLE) {
    return [t, h, l, b];
  }else{
    return [h, l, b];
  }
}

main();
