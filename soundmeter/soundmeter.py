#!/usr/bin/python

import sys
import os
import usb.core
import time
import socket
from influxdb_client import Point, InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS, PointSettings

client = InfluxDBClient.from_env_properties()

bucket = os.getenv('INFLUXDB_V2_BUCKET', 'bucket')
hostname = socket.gethostname()

point_settings = PointSettings()
point_settings.add_default_tag("host", hostname)
point_settings.add_default_tag("device", "WS1361")

tags = ["BALENA", "BALENA_SERVICE_NAME", "BALENA_APP_NAME", "BALENA_DEVICE_TYPE"]
for tag in tags:
    val = os.getenv(tag, False)
    if val:
        point_settings.add_default_tag(tag, val)

write_api = client.write_api(write_options=SYNCHRONOUS, point_settings=point_settings)

dev = usb.core.find(idVendor = 0x16c0, idProduct = 0x5dc)
assert dev is not None

while True:
    ret = dev.ctrl_transfer(0xC0, 4, 0, 0, 200)
    dB = (ret[0] + ((ret[1] & 3) * 256)) * 0.1 + 30

    p = Point("sound").field("db", float(dB))
    try:
        write_api.write(bucket=bucket, record=p)
    except Exception as err:
        print('Exception submitting metrics: %s.', err)
        pass

    time.sleep(1)
