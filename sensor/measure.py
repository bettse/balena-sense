import os
import time
import socket
from sense_hat import SenseHat
from influxdb_client import Point, InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS, PointSettings
from subprocess import PIPE, Popen

client = InfluxDBClient.from_env_properties()

bucket = os.getenv('INFLUXDB_V2_BUCKET', 'bucket')
hostname = socket.gethostname()

point_settings = PointSettings()
point_settings.add_default_tag("host", hostname)
point_settings.add_default_tag("device", "SenseHat")

tags = ["BALENA", "BALENA_SERVICE_NAME", "BALENA_APP_NAME", "BALENA_DEVICE_TYPE"]
for tag in tags:
    val = os.getenv(tag, False)
    if val:
        point_settings.add_default_tag(tag, val)

write_api = client.write_api(write_options=SYNCHRONOUS, point_settings=point_settings)

sense = SenseHat()

# Get the temperature of the CPU for compensation
def get_cpu_temperature():
    process = Popen(['vcgencmd', 'measure_temp'], stdout=PIPE, universal_newlines=True)
    output, _error = process.communicate()
    return float(output[output.index('=') + 1:output.rindex("'")])


# Tuning factor for temperature compensation. Decrease this number to adjust the
# temperature down, and increase to adjust up
factor = float(os.getenv("TEMP_FACTOR", 1.0))

cpu_temps = [get_cpu_temperature()] * 5

while 1:
    cpu_temp = get_cpu_temperature()
    cpu_temps = cpu_temps[1:] + [cpu_temp]
    avg_cpu_temp = sum(cpu_temps) / float(len(cpu_temps))

    raw_temp = sense.temperature
    temperature = raw_temp - ((avg_cpu_temp - raw_temp) / factor)

    t = Point("temperature").field("celsius", float(temperature)).field("fahrenheit", float(temperature) * 9/5 + 32)
    h = Point("humidity").field("value", float(sense.humidity))
    p = Point("pressure").field("value", float(sense.pressure))

    try:
        write_api.write(bucket=bucket, record=[t, h, p])
    except Exception as err:
        print('Exception submitting metrics: %s.', err)
        pass

    time.sleep(1)
