# Pi Hat Sense

Runs one container to record sensor metrics and report them to influx host.  Runs a second container with a simple flask webserver to write messages to the LED grid.

## Sensor

Readings are taken from the humidity, temperature and barometric pressure sensors once every 10 seconds.
env `INFLUX_HOST` to control where they are sent

## Webserver

Routes:
 * GET `/`: returns "Hello, world!"
 * POST `/message`. Takes json body and controls message and low_light setting of Pi sense LED matrix.
     * `message` (text) the message ot be displayed
     * `low_light` (boolean) set low_light

